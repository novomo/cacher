const memcached = require("../memcache/memcache_connection");
const IP = require('ip');

const upload_error = require("../../node_error_functions/upload_error")
module.exports = async (keyType, keyId) => {
  try {
  await memcached.delete(`${keyType}_${keyId}`);
} catch(err) {
  console.log(err)
  upload_error({
    errorTitle: "Delete item from cache",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}
};
