const memcached = require("../memcache/memcache_connection");

module.exports = async (keyType, keyId, data) => {
  try {
  await memcached.set(
    `${keyType}_${keyId}`,
    JSON.stringify(data)
      .replace(/\\u[\dA-F]{4}/gi, (unicode) => {
        return String.fromCharCode(parseInt(unicode.replace(/\\u/g, ""), 16));
      })
      .replaceAll(/\n/g, " ")
      .replaceAll(/\r/g, " ")
      .replaceAll(/\t/g, " ")
  );
} catch(err) {
  console.log(err)
  upload_error({
    errorTitle: "Updating item from cache",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}
};
